//A server program to handle multiple clients that connects using websocket module
//Importing websocket module
const web_socket = require('ws');
//Defining port number
const port_number = 1234;
//Initializing server
const ws_server = new web_socket.Server({ port: port_number });
// Initialization of list of client,id and start time
var clients = [];
var client_id = 0;
var start_time = 0;
console.log('Server started waiting for client!!');
ws_server.on('connection', function (ws) {
    //Server is open to accept client
    //Appending new clients to the list via id and websocket
    clients.push({ id: (++client_id), channel: ws });
    //A function call to acquire client list via client id
    client_by_id();
    start_time = new Date().getTime();
    console.log('Start time', start_time);
    ws.on('message', function (message) {
        //Recieve messages
        //Calculations of latency(difference between request and response time of message event)
        latency = new Date().getTime() - start_time;
        console.log('Client sent:', message);
        console.log('Latency in milliseconds:', latency);
    })
    //Send messages
    //multiple messages can be sent
    ws.send('Hi client your id is:' + client_id);
    ws.send('Hello client ' + client_id);
    //On close of any client this event triggers and client list will be updated
    ws.on('close', function () {
        console.log('Client is closed');
        for (i = 0; i < clients.length; i++) {
            if (clients[i].channel == this) {
                console.log('Closed client is:', clients[i].id);
                clients.splice(i, 1);
                client_by_id();
            }
        }
    })
    //If there is an error in the connection between server and client error event will be triggered
    ws.on('error', function (event) {
        console.error('There is an error occured', event);
    })
})
//This function will return the list of clients by its id
function client_by_id() {
    const client = [];
    clients.forEach(item => client.push(item.id));
    console.log('The clients connected are:', client);
}

