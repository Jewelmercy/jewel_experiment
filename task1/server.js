const WebSocket = require('ws')//adding websocket module

const wss = new WebSocket.Server({ port: 5678 })//defining server with port name 5678

wss.on('connection', (ws) => {
    //to recieve a message
    ws.on('message', function(message) {
        console.log('Received Message:', message)
      })
     //to send a message
    ws.onopen = () => {  
    ws.send('message to client');
    }
});
