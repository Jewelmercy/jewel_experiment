const WebSocket = require('ws')
const wss = new WebSocket.Server({ port: 9898 });
// creating list for clients
var clients = [];
var client_id = 0;
wss.on('connection', function(ws) {
    //assigning index using ws.id wher ws is object and id is variable in that object
    ws.id = client_id++;
    //assigning newly connected websockets as clients 
    clients[ws.id] = ws;
    ws.on('message', function(message) {
    console.log('client:', message);
    ws.send('Hi client your id is:'+ ws.id);
    ws.send('how are you client '+ws.id);
    });
    //to display all the clients connected
   //console.log(clients);
});