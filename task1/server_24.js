//A server program to handle multiple clients that connects using websocket module
//Importing websocket module
const web_socket = require('ws');
//Defining port number
const port_number=1234;
//Initializing server
const ws_server = new web_socket.Server({ port:port_number});
// Initialization of list of clients
var clients = [];
var client_id = 0;
console.log('Server started waiting for client!!');
ws_server.on('connection', function(ws) 
{
    //Server is open to accept client
    id = client_id++;
    //Appending new clients to the list via id and websocket
    clients.push({id:id, state:ws }); 
    console.log('the list of clients:',client_by_id());
    ws.on('message', function(message){
        //Recieve messages
        console.log('client sent:', message);
    });
    //Send messages
    //multiple messages can be sent
    ws.send('Hi client your id is:'+ id);
    ws.send('Hello client '+ id);
    //On close of any client this event triggers and client list will be updated
    ws.on('close',function(){
        console.log('client is closed');  
        for (i=0;i<clients.length;i++){
            if(clients[i].state==this){
                console.log('closed client is:',clients[i].id);
                clients.splice(i,1);
                console.log('the connected list of clients:',client_by_id());
            };
        };
    });
});
//This function will return the list of clients by its ID
function client_by_id(){
    var client=[];
    for(j=0;j<clients.length;j++){
        client.push(clients[j].id)
    };
    return client
};