Description on task1:
The task is divided into two subtasks, one is to create a server in node js/python and another is to create client in node js.

In order to run node.js files there should be ‘node’ installed in computer. 
This can be downloaded from, https://nodejs.org/dist/v10.16.3/node-v10.16.3-x64.msi .
If websockets(ws) module is not installed in node, please do install using command line ‘npm install ws’. 
To create a server a local host is used along with a port number. (Ex.ws://127.0.0.1:9898). 

Execution:

OS: windows 13.
Tool: Visual Studio Code (also need to download node extension for execution).
Programming language: Node.js. 
Steps to be carried out:

•	Server program has be saved as name.js file. And to execute ‘node name.js’ command is used at the terminal (here name is server).
•	Client is also written in node.js and can be executed on terminal using the command ‘node name.js’(here name is client).
•	For multiple clients can use the command ‘start node client.js’ which will run a new client on new window.
