#!/usr/bin/env python

# WS server example

import asyncio
import websockets

async def message(websocket, path):
    
    name = await websocket.recv()
    print(f" The recieved msg from server is: {name}")

    greeting = f"Hello client!"
    await websocket.send(greeting)
    print(f"Send a message to client: {greeting}")

start_server = websockets.serve(message, "127.0.0.1", 5678)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()